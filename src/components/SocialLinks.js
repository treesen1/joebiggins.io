import React from "react";
import {
	FaFacebookSquare,
  FaGithubSquare,
	FaEnvelope,
  FaTwitterSquare,
	FaLinkedinSquare,

} from "react-icons/lib/fa";
// https://gorangajic.github.io/react-icons/fa.html

const SocialLinks = () => (
  <ul className="social">
    <li>
      <a href="https://www.facebook.com/joe.biggins.35/">
        <FaFacebookSquare />
      </a>
    </li>
    <li>
      <a href="https://twitter.com/_jjbiggins">
        <FaTwitterSquare />
      </a>
    </li>
    <li>
      <a href="mailto:joseph-biggins@uiowa.edu?&cc=jjbiggins@joebiggins.io">
        <FaEnvelope />
      </a>
    </li>
    <li>
      <a href="https://linkedin.com/in/josephbiggins">
        <FaLinkedinSquare />
      </a>
    </li>
    <li>
      <a href="https://github.com/jjbiggins">
        <FaGithubSquare />
      </a>
    </li>
  </ul>
);

export default SocialLinks;
