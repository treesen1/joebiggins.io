import React from "react";
import SocialLinks from "./SocialLinks";

const Banner = () => (
  <div className="row banner">
    <div className="banner-text">
		<h2 className="responsive-headline">I'm </h2>
		<h1 className="responsive-headline">Joe Biggins.</h1>
      <h3>
        Hi, currently located in Chicago, but open to relocating. I'm a <span>software engineer </span>,
        <span>full-stack developer </span> and <span> web designer</span> looking for full time employment. Let's
        <a className="smoothscroll" href="#about">
          {" "}
          start scrolling
        </a>{" "}
        and learn more
        <a className="smoothscroll" href="#resume">
          {" "}
          About Me
        </a>.
      </h3>
      <hr />
      <SocialLinks />
    </div>
  </div>
);

export default Banner;
