import React from "react";
import { FaPlus, FaTag } from "react-icons/lib/fa";

import Coffee from "../assets/images/portfolio/sv.jpg";
import Retrocam from "../assets/images/portfolio/console.jpg";
import Judah from "../assets/images/portfolio/judah.jpg";
import IntoTheLight from "../assets/images/portfolio/into-the-light.jpg";
import Farmerboy from "../assets/images/portfolio/farmerboy.jpg";
import Girl from "../assets/images/portfolio/girl.jpg";
import Origami from "../assets/images/portfolio/origami.jpg";
import Console from "../assets/images/portfolio/modals/white-bosch-programmable-thermostats-bcc-100-64_1000.jpg";

import CoffeeModal from "../assets/images/portfolio/modals/m-sv.jpg";
import RetroModal from "../assets/images/portfolio/modals/m-console.jpg";
import JudahModal from "../assets/images/portfolio/modals/m-judah.jpg";
import IntoTheLightModal from "../assets/images/portfolio/modals/m-intothelight.jpg";
import FarmerboyModal from "../assets/images/portfolio/modals/m-farmerboy.jpg";
import GirlModal from "../assets/images/portfolio/modals/m-girl.jpg";
import OrigamiModal from "../assets/images/portfolio/modals/m-origami.jpg";
import ConsoleModal from "../assets/images/portfolio/modals/white-bosch-programmable-thermostats-bcc-100-64_1000.jpg";

const Portfolio = () => (
  <section id="portfolio">
    <div className="row">
      <div className="twelve columns collapsed">
        <h1>Check Out Some of My Projects.</h1>

        <div
          id="portfolio-wrapper"
          className="bgrid-quarters s-bgrid-thirds cf"
        >
          <div className="columns portfolio-item">
            <div className="item-wrap">
              <a href="#modal-01" title="SoundVillage">
                <img alt="" src={Coffee} />
                <div className="overlay">
                  <div className="portfolio-item-meta">
                    <h5>SoundVillage</h5>
                    <p>Development</p>
                  </div>
                </div>
                <div className="link-icon">
                  <FaPlus />
                </div>
              </a>
            </div>
          </div>

          <div className="columns portfolio-item">
            <div className="item-wrap">
              <a href="#modal-02" title="">
                <img alt="" src={Console} />
                <div className="overlay">
                  <div className="portfolio-item-meta">
                    <h5>Home Automation Thermostat</h5>
                    <p>Full-Stack Dev</p>
                  </div>
                </div>
                <div className="link-icon">
                  <FaPlus />
                </div>
              </a>
            </div>
          </div>





          <div className="columns portfolio-item">
            <div className="item-wrap">
              <a href="#modal-05" title="">
                <img alt="" src={Farmerboy} />
                <div className="overlay">
                  <div className="portfolio-item-meta">
                    <h5>Self-Driving Robot</h5>
                    <p>Robotics</p>
                  </div>
                </div>
                <div className="link-icon">
                  <FaPlus />
                </div>
              </a>
            </div>
          </div>





          <div className="columns portfolio-item">
            <div className="item-wrap">
              <a href="#modal-08" title="">
                <img alt="" src={Retrocam} />
                <div className="overlay">
                  <div className="portfolio-item-meta">
                    <h5>Retrocam</h5>
                    <p>Web Development</p>
                  </div>
                </div>
                <div className="link-icon">
                  <FaPlus />
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>

      <div id="modal-01" className="popup-modal mfp-hide">
        <img
          className="scale-with-grid"
          src={CoffeeModal}
          alt=""
        />

        <div className="description-box">
          <h4>SoundVillage</h4>
          <p>
			  Designed cross-platform React-Native application to allow users to natively create, share, collaborate, and play real-time, dynamic playlists.
			  Built up-vote and down-vote capabilities; automatically playing the song that had received the most votes next in the queue upon the previous songs completion
			  from Firebase database–complimenting the mobile application.
		  </p>
          <span className="categories">
            <FaTag /> Lead Designer, Developer
          </span>
        </div>

        <div className="link-box">
          <a href="https://github.com/jjbiggins/SoundVillage">Details</a>
          <a href="#portfolio" className="popup-modal-dismiss">Close</a>
        </div>
      </div>

      <div id="modal-02" className="popup-modal mfp-hide">
        <img
          className="scale-with-grid"
          src={ConsoleModal}
          alt=""
        />

        <div className="description-box">
          <h4>Home-Automation-Thermostat</h4>
          <p>
			  Developed a UI for Web and Adafruit GFX touchscreen; providing user controls to customize system configurations for temperature, time, HVAC mode, HVAC status, and programmable set points.
			  Implemented full stack solution using Node.js to create our server with Javascript and Jade comprising the client.
          </p>
          <span className="categories">
            <FaTag />Branding, Web Development
          </span>
        </div>

        <div className="link-box">
          <a href="http://www.behance.net">Details</a>
          <a href="#portfolio" className="popup-modal-dismiss">Close</a>
        </div>
      </div>

      <div id="modal-03" className="popup-modal mfp-hide">
        <img
          className="scale-with-grid"
          src={JudahModal}
          alt=""
        />

        <div className="description-box">
          <h4>Autonomous Robotics Car</h4>
          <p>
			  Designed and developed complete end-to-end intelligence and automation to execute driving instructions precisely, and efficiently during single lap race.
		  </p>
          <span className="categories">
            <FaTag />Branding
          </span>
        </div>

        <div className="link-box">
          <a href="http://www.behance.net">Details</a>
          <a href="#portfolio" className="popup-modal-dismiss">Close</a>
        </div>
      </div>

      <div id="modal-04" className="popup-modal mfp-hide">
        <img
          className="scale-with-grid"
          src={IntoTheLightModal}
          alt=""
        />

        <div className="description-box">
          <h4>Into the Light</h4>
          <p>
            Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin,
            lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis
            sem nibh id elit.
          </p>
          <span className="categories">
            <FaTag />Photography
          </span>
        </div>

        <div className="link-box">
          <a href="http://www.behance.net">Details</a>
          <a href="#portfolio" className="popup-modal-dismiss">Close</a>
        </div>
      </div>
    </div>
  </section>
);

export default Portfolio;
